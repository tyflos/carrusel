# Accessible carrusel

## Description

This is a basic example of a 3D interface using *SceneKit* with accessibility suport.
This project was created from the basic SceneKit project template in xCode 6.

## License

© Copyright 2014 Jonathan Chacón Barbero
This project is licensed under the Apache License, Version 2.0. You may 
obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an ìAS ISî BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

This software includes media resources from Apple and Tyflos Accessible Software.