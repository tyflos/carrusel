//
//  main.m
//  AccessibleCarrusel
//
//  Created by Jonathan Chacon Barbero on 25/10/14.
//  Copyright (c) 2014 Tyflos Accessible Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
