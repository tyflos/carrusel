//
//  GameViewController.h
//  AccessibleCarrusel
//

//  Copyright (c) 2014 Tyflos Accessible Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SceneKit/SceneKit.h>

@interface GameViewController : UIViewController

@property (nonatomic, strong) SCNScene *scene;
@property (nonatomic, strong) SCNNode *carrusel;

@end
