//
//  AppDelegate.h
//  AccessibleCarrusel
//
//  Created by Jonathan Chacon Barbero on 25/10/14.
//  Copyright (c) 2014 Tyflos Accessible Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

