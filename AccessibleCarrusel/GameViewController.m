//
//  GameViewController.m
//  AccessibleCarrusel
//
//  Created by Jonathan Chacon Barbero on 25/10/14.
//  Copyright (c) 2014 Tyflos Accessible Software. All rights reserved.
//

#import "GameViewController.h"

#define PI 3.141595f

@implementation GameViewController
@synthesize scene = scene;


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createScene];
    [self createLogo];
    [self createCarrusel];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Events management

- (void) handleTap:(UIGestureRecognizer*)gestureRecognize
{
    // retrieve the SCNView
    SCNView *scnView = (SCNView *)self.view;
    
    // check what nodes are tapped
    CGPoint p = [gestureRecognize locationInView:scnView];
    NSArray *hitResults = [scnView hitTest:p options:nil];
    
    // check that we clicked on at least one object
    if([hitResults count] > 0){
        // retrieved the first clicked object
        SCNHitTestResult *result = [hitResults objectAtIndex:0];
        SCNNode* resultNode = result.node;
        
        if ([resultNode.name rangeOfString:@"http://"].location != NSNotFound) {
            [self openItemWithUrl:resultNode.name];
        }
    }
}

- (void) openItemWithUrl:(NSString*) strUrl {
    NSURL *url = [NSURL URLWithString:strUrl];
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark - SceneKit management

- (void) createScene {
    // create a new scene
    scene = [SCNScene scene];
    
    // create and add a camera to the scene
    SCNNode *cameraNode = [SCNNode node];
    cameraNode.camera = [SCNCamera camera];
    [scene.rootNode addChildNode:cameraNode];
    
    // place the camera
    cameraNode.position = SCNVector3Make(0, 6, 30);
    cameraNode.rotation = SCNVector4Make(1, 0, 0,-1 * PI / 16);
    
    // create and add lights to the scene
    SCNNode *lightNode = [SCNNode node];
    lightNode.light = [SCNLight light];
    lightNode.light.type = SCNLightTypeOmni;
    lightNode.position = cameraNode.position;
    [scene.rootNode addChildNode:lightNode];

    // create and add an ambient light to the scene
    SCNNode *ambientLightNode = [SCNNode node];
    ambientLightNode.light = [SCNLight light];
    ambientLightNode.light.type = SCNLightTypeAmbient;
    ambientLightNode.light.color = [UIColor darkGrayColor];
    [scene.rootNode addChildNode:ambientLightNode];
    
    // retrieve the SCNView
    SCNView *scnView = (SCNView *)self.view;
    // set the scene to the view
    scnView.scene = scene;
    
    
    // allows the user to manipulate the camera
    scnView.allowsCameraControl = YES;
    
    
    
    // configure the view
    scnView.backgroundColor = [UIColor blackColor];
    
    // add a tap gesture recognizer
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    NSMutableArray *gestureRecognizers = [NSMutableArray array];
    [gestureRecognizers addObject:tapGesture];
    [gestureRecognizers addObjectsFromArray:scnView.gestureRecognizers];
    scnView.gestureRecognizers = gestureRecognizers;
}

- (void) createLogo {
    SCNNode* ball = [SCNNode node];
    ball.geometry = [SCNSphere sphereWithRadius:2];
    ball.position = SCNVector3Make(0, -3, 5);
    
    // define the texture for the ball
    SCNMaterial *ballMaterial = [SCNMaterial material];
    ballMaterial.diffuse.contents = [UIImage imageNamed:@"logo.png"];
    ball.geometry.firstMaterial = ballMaterial;
    
    // Add a basic animation
    [ball runAction:[SCNAction repeatActionForever:[SCNAction rotateByX:0.2 y:1 z:0 duration:5]]];
    
    [scene.rootNode addChildNode:ball];
    
    // make ball invisible for accessibility
    ball.isAccessibilityElement = NO;
}

- (void) createCarrusel {
    _carrusel = [SCNNode node];
    _carrusel.position = SCNVector3Make(0, -3, 5);
    
    // Textures
    SCNMaterial *tragaperrasMaterial = [SCNMaterial material];
    tragaperrasMaterial.diffuse.contents = [UIImage imageNamed:@"tragaperras.png"];
    SCNMaterial *buscaminasMaterial = [SCNMaterial material];
    buscaminasMaterial.diffuse.contents = [UIImage imageNamed:@"buscaminas.png"];
    SCNMaterial *othelloMaterial = [SCNMaterial material];
    othelloMaterial.diffuse.contents = [UIImage imageNamed:@"othello.png"];
    SCNMaterial *pacMaterial = [SCNMaterial material];
    pacMaterial.diffuse.contents = [UIImage imageNamed:@"programaraciegas.png"];
    
    SCNNode *panelBuscaminas = [SCNNode node];
    panelBuscaminas.name = @"http://www.appstore.com/accessibleminesweeper";
    panelBuscaminas.position = SCNVector3Make(0, 0, 5);
    panelBuscaminas.geometry = [SCNBox boxWithWidth:8 height:8 length:0.01 chamferRadius:0.05];
    panelBuscaminas.geometry.firstMaterial = buscaminasMaterial;
    
    SCNNode *panelTragaperras = [SCNNode node];
    panelTragaperras.name = @"http://www.appstore.com/accessiblefruitmachine";
    panelTragaperras.position = SCNVector3Make(5, 0, 0);
    panelTragaperras.geometry = [SCNBox boxWithWidth:8 height:8 length:0.01 chamferRadius:0.05];
    panelTragaperras.geometry.firstMaterial = tragaperrasMaterial;
    panelTragaperras.rotation = SCNVector4Make(0, 1, 0, (2 * PI / 4) * 1);
    
    SCNNode *panelOthello = [SCNNode node];
    panelOthello.name = @"http://www.appstore.com/accessibleothello";
    panelOthello.position = SCNVector3Make(0, 0, -5);
    panelOthello.geometry = [SCNBox boxWithWidth:8 height:8 length:0.01 chamferRadius:0.05];
    panelOthello.geometry.firstMaterial = othelloMaterial;
    panelOthello.rotation = SCNVector4Make(0, 1, 0, (2 * PI / 4) * 2);
    
    SCNNode *panelPAC = [SCNNode node];
    panelPAC.name = @"http://www.appstore.com/programaraciegasrss";
    panelPAC.position = SCNVector3Make(-5, 0, 0);
    panelPAC.geometry = [SCNBox boxWithWidth:8 height:8 length:0.01 chamferRadius:0.05];
    panelPAC.geometry.firstMaterial = pacMaterial;
    panelPAC.rotation = SCNVector4Make(0, 1, 0, (2 * PI / 4) * 3);
    
    
    [_carrusel addChildNode:panelBuscaminas];
    [_carrusel addChildNode:panelTragaperras];
    [_carrusel addChildNode:panelOthello];
    [_carrusel addChildNode:panelPAC];
    [scene.rootNode addChildNode:_carrusel];
    
    [_carrusel runAction:[SCNAction repeatActionForever:[SCNAction rotateByX:0 y:1 z:0 duration:5]]];
    
    // make panels accessible
    panelBuscaminas.isAccessibilityElement = YES;
    panelOthello.isAccessibilityElement = YES;
    panelTragaperras.isAccessibilityElement = YES;
    panelPAC.isAccessibilityElement = YES;
    panelBuscaminas.accessibilityLabel = @"Buscaminas accesible";
    panelTragaperras.accessibilityLabel = @"Tragaperras accesible";
    panelOthello.accessibilityLabel = @"Othelo accesible";
    panelPAC.accessibilityLabel = @"Programar a ciegas RSS";
    
    
}
@end
